# Wikipetia | EJCM

Seja muito bem vinde ao Wikipetia, site que faz sátira ao famoso almanaque virtual Wikipédia. O Wikipetia foi criado não só com o objetivo de promover o cargo desse pobre dev back-end que vos fala, mas como também para trazer momentos de relaxamento para os membros da EJCM. Mostrando imagens de cães e gatos, o site tem o intuito de aliviar um pouco do estresse rotineiro por conta da faculdade e dos projetos.
 
**Status do Projeto** : Terminado

![Badge](https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white)
![Badge](https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white)
![Badge](https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white)
![Badge](https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB)
 
## Tabela de Conteúdo

 1. [Tecnologias utilizadas](#tecnologias-utilizadas)
 2. [Instalação](#instalação)
 3. [Uso](#uso)
 4. [Arquitetura](#arquitetura)
 5. [Autores](#autores)
 
## Tecnologias utilizadas

 - [React](https://pt-br.reactjs.org/), ^18.2.0
 - [Typescript](https://www.typescriptlang.org/), ^4.8.2
 - [Axios](https://axios-http.com/ptbr/docs/intro)
 - [The Dog API](https://thedogapi.com/)
 - [The Cat API](https://thecatapi.com/)

## Instalação 

- Clone este repositório:

``` bash
$ git clone https://gitlab.com/ormesino/wikipetia.git
```

- Vá até a pasta onde está o projeto clonado e abra o terminal nela;
- Digite o comando a seguir:

``` bash
$ npm install
```

## Uso

Para o uso do site, basta executar o seguinte comando no terminal a partir da pasta do projeto:

``` bash
$ npm start
```

## Arquitetura
- [Figma](https://www.figma.com/file/54KbI5zIwVekZj7isuzHUq/Wikipetia?node-id=0%3A1)

## Autor
* Dev Back-end - Pedro Mateus
## Última atualização: 16/09/2022
